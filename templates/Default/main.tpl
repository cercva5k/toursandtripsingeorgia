<!DOCTYPE html>
<html[available=lostpassword|register] class="page_form_style"[/available] lang="ru">
<head>
	{headers}
	<meta name="HandheldFriendly" content="true">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="user-scalable=0, initial-scale=1.0, maximum-scale=1.0, width=device-width"> 
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">

	<link rel="shortcut icon" href="{THEME}/images/favicon.ico">
	<link rel="apple-touch-icon" href="{THEME}/images/touch-icon-iphone.png">
	<link rel="apple-touch-icon" sizes="76x76" href="{THEME}/images/touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="{THEME}/images/touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="{THEME}/images/touch-icon-ipad-retina.png">

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link href="{THEME}/css/engine.css" type="text/css" rel="stylesheet">
	<!-- <link href="{THEME}/css/styles.css" type="text/css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="{THEME}/css/random.css">
	<link rel="stylesheet" type="text/css" href="{THEME}/css/app.css">
</head>
<body>
	{include file='/component/navbar.tpl'}
	[available=main]
	{include file='/component/carousel.tpl'}
	<div class="block-headline container">
		<h1 class="primary_title">Popular Tours</h1>
		<span class="primary_text">From Historical cities to natural splendours, come see the best of world!</span>
	</div>
	{include file='/component/popular_tours.tpl'}
	<div class="block-headline container">
		<h1 class="primary_title">Special Offers</h1>
		<span class="primary_text">From Historical cities to natural splendours, come see the best of world!</span>
	</div>
	{include file='/component/special_offer.tpl'}
	<div class="block-headline container">
		<h1 class="primary_title">Recent Tours</h1>
		<span class="primary_text">From Historical cities to natural splendours, come see the best of world!</span>
	</div>
	{include file='/component/tours_inmain.tpl'}
	<div class="block-headline container">
		<h1 class="primary_title">Travel News</h1>
		<span class="primary_text">From Historical cities to natural splendours, come see the best of world!</span>
	</div>
	{include file='/component/travel_news_block.tpl'}
	[/available]
	[not-available=main]
	<div class="container">
			{content}
	</div>
	[/not-available]
	{AJAX}
	{include file='/component/footer.tpl'}
	<!-- <script src="{THEME}/js/lib.js"></script> -->
	<script src="{THEME}/js/app.js"></script>
	<script src="{THEME}/js/bootstrap.min.js"></script>
	<script>
		jQuery(function($){
			$.get("{THEME}/images/sprite.svg", function(data) {
			  var div = document.createElement("div");
			  div.innerHTML = new XMLSerializer().serializeToString(data.documentElement);
			  document.body.insertBefore(div, document.body.childNodes[0]);
			});
			$('#topcarousel .carousel-inner .carousel-item:first-child').addClass("active")
		});
	</script>
	[available=main]
	<style>
		.navbar {
			position: absolute;
			background: transparent;
			left: 50%;
    		transform: translateX(-50%);
		}

		.navbar .nav-link {
			color: #FFF !important;
		}
	</style>
	[/available]
</body>
</html>