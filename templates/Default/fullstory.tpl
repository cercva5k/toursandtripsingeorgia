<article class="full_tours">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-10 full_cover mx-auto">
				[xfvalue_cover]
				<div class="rating_like">
					[rating-type-2]
						[rating-plus]
						<svg class="bi bi-heart-fill" width="24px" height="24px" viewBox="0 0 20 20" fill="#FF5555" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
						  </svg>
							{rating}
						[/rating-plus]
					[/rating-type-2]
				</div>
				[xfgiven_price]<span class="price_on_cover">Price: [xfvalue_price]$</span>[/xfgiven_price]
			</div>
			<span class="date">{date=d F Y}</span>
			<div class="col-12 col-md-9 full_title mx-auto">
				<h1>{title}</h1>
			</div>
		</div>
		<div class="row we_offer">
			<span class="col-12 col-md-12 we-offer-headline">We Offer</span>
			<div class="col-12 col-md-8 offer_list mx-auto">
				<div class="row">
					<div class="col-6 col-md-3 offer_item">
						<img src="{THEME}/images/icons/food.png" alt="Food">
						Food
					</div>
					<div class="col-6 col-md-3 offer_item">
						<img src="{THEME}/images/icons/wifi.png" alt="Wi-Fi">
						Wi-Fi
					</div>
					<div class="col-6 col-md-3 offer_item">
						<img src="{THEME}/images/icons/transport.png" alt="Transport">
						Transport
					</div>
					<div class="col-6 col-md-3 offer_item">
						<img src="{THEME}/images/icons/guide.png" alt="Guide Service">
						Guide Service
					</div>
					<div class="col-6 col-md-3 offer_item">
						<img src="{THEME}/images/icons/hotel.png" alt="Hotel">
						Hotel
					</div>
					<div class="col-6 col-md-3 offer_item">
						<img src="{THEME}/images/icons/museum.png" alt="Museum tickets">
						Museum tickets
					</div>
				</div>
			</div>
			<div class="col-12 col-md-8 mx-auto mt-5">
				<div class="row book_types">
					<div class="col-12 col-md-12 book_types_text">
						<strong>Book Tours</strong>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="whatsapp_book mx-auto">
							<img src="{THEME}/images/icons/whatsApp.png" alt="">
							<span class="book_types_in_text">WhatsApp</span>
						</a>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="viber_book mx-auto">
							<img src="{THEME}/images/icons/viber.png" alt="">
							<span class="book_types_in_text">Viber</span>
						</a>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="facebook_book mx-auto">
							<img src="{THEME}/images/icons/facebook.png" alt="">
							<span class="book_types_in_text">Facebook</span>
						</a>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="booking_book mx-auto">
							<img src="{THEME}/images/icons/booking.png" alt="">
						</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-8 highlight mx-auto">
				<div class="row">
					<div class="col-12 col-md-12 highlight_body">{full-story}</div>
					<div class="col-12 col-md-12 mx-auto tours_gallery">
						<div class="row">
							<span class="col-12 col-md-12 gallery_headline">Gallery:</span>
							<div class="col-12 col-md-12">
								[xfvalue_gallery]
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-8 mx-auto mt-5">
				<div class="row book_types">
					<div class="col-12 col-md-12 book_types_text">
						<strong>Book Tours</strong>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="whatsapp_book mx-auto">
							<img src="{THEME}/images/icons/whatsApp.png" alt="">
							<span class="book_types_in_text">WhatsApp</span>
						</a>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="viber_book mx-auto">
							<img src="{THEME}/images/icons/viber.png" alt="">
							<span class="book_types_in_text">Viber</span>
						</a>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="facebook_book mx-auto">
							<img src="{THEME}/images/icons/facebook.png" alt="">
							<span class="book_types_in_text">Facebook</span>
						</a>
					</div>
					<div class="col-12 col-md-3">
						<a href="#" target="_blank" class="booking_book mx-auto">
							<img src="{THEME}/images/icons/booking.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-12 col-md-8 mx-auto">{comments}</div>
			<div class="col-12 col-md-8 mx-auto mt-5">{addcomments}</div>
		</div>
	</div>
	<div class="bg_light_fluid">
		<div class="container">
			<div class="row">
				[catlist=3] 
					{custom category="3" template="shortstory" from="0" limit="4" order="view" sort="desc" cache="no"}
				[/catlist]
				[catlist=4,5] 
					{custom category="4" template="component/standart_news_card" from="0" limit="4" order="view" sort="desc" cache="no"}
				[/catlist]
			</div>
		</div>
	</div>
</article>