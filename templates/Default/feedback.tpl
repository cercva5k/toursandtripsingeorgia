<article class="container my-5">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<div class="row mb-5">
				<div class="col-12 col-md-4">
					<a href="#" target="_blank" class="whatsapp_book mx-auto">
						<img src="{THEME}/images/icons/whatsApp.png" alt="">
						<span class="book_types_in_text">WhatsApp</span>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" target="_blank" class="viber_book mx-auto">
						<img src="{THEME}/images/icons/viber.png" alt="">
						<span class="book_types_in_text">Viber</span>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" target="_blank" class="facebook_book mx-auto">
						<img src="{THEME}/images/icons/facebook.png" alt="">
						<span class="book_types_in_text">Facebook</span>
					</a>
				</div>
			</div>
			<div class="col-12 col-md-12">
				<div class="or-contact mx-auto">or</div>
	<div class="box_in">
		<div class="addform">
			<ul class="ui-form">
			[not-logged]
			<li class="form-group row">
				<div class="col-12 col-md-6"><input placeholder="Fullname" type="text" maxlength="35" name="name" id="name" class="wide" required autocomplete="off"></div>
				<div class="col-12 col-md-6"><input placeholder="Your E-mail" type="email" maxlength="35" name="email" id="email" class="wide" required autocomplete="off"></div>
			</li>
			[/not-logged]
				<div class="row">
					<li class="form-group col-12 col-md-6">
						<input placeholder="Subject" type="text" maxlength="45" name="subject" id="subject" class="wide" required autocomplete="off">
					</li>
					<li class="form-group col-12 col-md-6">
						{recipient}
					</li>
				</div>
				<li class="form-group">
					<textarea placeholder="Message" name="message" id="message" rows="8" class="wide" required></textarea>
				</li>
			[attachments]
				<li class="form-group">
					<label for="question_answer">File:</label>
					<input name="attachments[]" type="file" multiple>
				</li>
			[/attachments]
			[recaptcha]
				<li class="form-group">{recaptcha}</li>
			[/recaptcha]
			</ul>
			<div class="form_submit">
				<button class="btn btn-big" type="submit" name="send_btn"><b>Submit</b></button>
			</div>
		</div>
	</div>
			</div>
		</div>
	</div>
</article>