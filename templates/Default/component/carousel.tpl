<div id="topcarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="2000">
    <div class="carousel-inner">
        <div class="block-on-top-carousel container">
            <h1>Lets’s Make Your Best Trip Ever</h1>
            <span>The best travel for your journey respectful of the environment  </span>
            <a href="#" class="top-carousel-button">Book Now</a>
            <ol class="carousel-indicators ml-auto" style="margin-top: -150px;">
                <li data-target="#topcarousel" data-slide-to="0" class="active"></li>
                <li data-target="#topcarousel" data-slide-to="1"></li>
                <li data-target="#topcarousel" data-slide-to="2"></li>
                <li data-target="#topcarousel" data-slide-to="3"></li>
                <li data-target="#topcarousel" data-slide-to="4"></li>
            </ol>
        </div>
        <div class="inner-slider">
        {custom category="9" template="component/carousel_item" available="main" from="0" limit="5" order="date" sort="desc" cache="no"}
        </div>
    </div>
  </div>