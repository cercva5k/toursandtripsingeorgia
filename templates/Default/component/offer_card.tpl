<div class="col-12 offer_card">
    <div class="offer_card_cover">
        [xfvalue_cover]
    </div>
    <div class="offer_card_body">
        <a href="{category-url}" class="offer_category">{category}</a>
        <a href="{full-link}">
            <h1 class="offer_card_title">{title}</h1>
        </a>
        <div class="offer_card_bottom">
            <div class="offer_list">
                <div class="d-flex align-items-center">
                    <img src="{THEME}/images/icons/price.png" alt="">
                    <div class="offer_price">[xfvalue_price]</div>
                </div>
                <div class="d-flex align-items-center">
                    <img src="{THEME}/images/icons/time.png" alt="">
                    [xfvalue_day] day
                </div>
                <div class="d-flex align-items-center">
                    <img src="{THEME}/images/icons/gallery.png" alt="">
                    [xfvalue_gallery_items] items
                </div>
            </div>
            <div class="rating_like">
                [rating-type-2]
                    [rating-plus]
                    <svg class="bi bi-heart-fill" width="24px" height="24px" viewBox="0 0 20 20" fill="#FF5555" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                      </svg>
                        {rating}
                    [/rating-plus]
                [/rating-type-2]
            </div>
        </div>
    </div>
</div>