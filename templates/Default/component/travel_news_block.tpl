<div class="bg_light_fluid">
    <div class="container">
        <div class="row">
            {custom category="5,4" template="component/standart_news_card" available="main" from="0" limit="12" order="date" sort="desc" cache="no"}
        </div>
        <a href="/blog" class="link_category mx-auto">All News</a>
    </div>
</div>