<div class="bg_light_fluid">
    <div class="special_offer_carousel container">
        <div class="offer_arrows">
            <div class="offer_prev">
                <img src="{THEME}/images/icons/arrow_prev.svg" alt="Prev">
            </div>
            <div class="offer_next">
                <img src="{THEME}/images/icons/arrow_next.svg" alt="Next">
            </div>
        </div>
        <div class="special_offer_carousel_inner col-12 col-md-10 mx-auto">
        {custom category="3" template="component/offer_card" available="main" from="0" limit="10" order="date" sort="desc" cache="no"}
        </div>
    </div>
</div>