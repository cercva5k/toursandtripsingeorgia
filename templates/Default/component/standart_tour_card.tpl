<div class="col-12 col-md-3">
    <div class="standart_tour_card">
        <div class="standart_card_top">
            <a href="{full-link}">
                <h1 class="standart_card_title">{title}</h1>
            </a>
            <span class="standart_card_tour_lang">[xfvalue_lang]</span>
        </div>
        <div class="standart_card_cover">
            [xfvalue_cover]
        </div>
        <div class="standart_card_count">
            <div class="gallery_count">
                <img src="{THEME}/images/icons/gallery.png" alt="gallery" class="mr-2">
                [xfvalue_gallery_items] Items
            </div>
            <div class="time_count d-flex align-items-center">
                <img src="{THEME}/images/icons/time.png" alt="" class="mr-2">
                [xfvalue_day] Day
            </div>
        </div>
        <div class="standart_tour_information">
            <div class="standart-card-information-card">
                <span class="this_title">Download</span>
                <span class="this_bottom">{views}</span>
            </div>
            <div class="standart-card-information-card">
                <span class="this_title">Revenue</span>
                <span class="this_bottom">[xfvalue_price]$</span>
            </div>
            <div class="standart-card-information-card">
                <span class="this_title">Reviews</span>
                <span class="this_bottom">{comments-num}</span>
            </div>
        </div>
        <a href="{full-link}" class="moredetails">
            View Details
            <svg class="bi bi-arrow-right" width="1em" height="1em" viewBox="0 0 16 16" fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z"/>
                <path fill-rule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z"/>
              </svg>
        </a>
    </div>
</div>