<footer>
    <div class="container footer_primary">
        <div class="row justify-content-between">
            <div class="col-12 col-md-auto">
                <a href="/">
                <img src="{THEME}/images/logo.png" alt=""></a>
            </div>
            <div class="col-12 col-md-auto footer_contact">
                <strong>Contact Us</strong>
                <ul>
                    <li>
                        <a href="mailto:info@toursandtripsingeorgia.com">E-Mail: info@toursandtripsingeorgia.com</a>
                    </li>
                    <li>
                        <a href="tel:+995 598 87 78 78">Phone: +995 598 87 78 78</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-auto footer_social">
                <strong>Follow Us:</strong>
                <ul>
                    <li>
                        <a href="#" target="_blank">
                            <img src="{THEME}/images/icons/twitter.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <img src="{THEME}/images/icons/instagram.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <img src="{THEME}/images/icons/facebook.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer_line"></div>
    <div class="container footer_foot">
        <span class="copyright">All Rights Reserved &copy; toursandtripsingeorgia.com</span>
        <a href="https://www.facebook.com/goartweb/" target="_blank" class="crafted">crafted by Go'Art</a>
    </div>
</footer>