<div class="top_travel_card col-12 col-md-11">
    <div class="top_travel_card_cover">
        <a href="{full-link}" class="top_card_details">Book Now</a>
        [xfvalue_cover]
    </div>
    <div class="top_travel_card_body">
        <a href="{full-link}">
            <h1 class="card_title">{title}</h1>
        </a>
        <div class="card_body">{short-story}</div>
        <div class="top_tours_info">
            <div class="rating_like">
                [rating-type-2]
                    [rating-plus]
                    <svg class="bi bi-heart-fill" width="24px" height="24px" viewBox="0 0 20 20" fill="#FF5555" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                      </svg>
                        {rating}
                    [/rating-plus]
                [/rating-type-2]
            </div>
            <a href="{full-link}#book" class="price">[xfvalue_price]$</a>
        </div>
    </div>
</div>