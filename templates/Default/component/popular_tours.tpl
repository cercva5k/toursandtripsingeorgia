<div class="bg_light_fluid">
    <div class="top_tours_container">
        <div class="container">
            <div class="row top_tours_carousel">
                {custom category="3" template="component/top_card" available="main" from="0" limit="10" order="view" sort="desc" cache="no"}
            </div>
            <div class="top_tours_arrows">
                <div class="top_tours_prev">
                    <img src="{THEME}/images/icons/arrow_prev.svg" alt="Prev">
                </div>
                <div class="top_tours_next">
                    <img src="{THEME}/images/icons/arrow_next.svg" alt="Next">
                </div>
            </div>
        </div>
    </div>
</div>