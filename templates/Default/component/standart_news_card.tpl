<div class="col-12 col-md-3">
    <div class="standart_news_card">
        <div class="standart_news_card_cover">
            [xfvalue_cover]
        </div>
        <h1 class="standart_news_card_title">{title}</h1>
        <div class="standart_card_body">{short-story limit="200"}</div>
        <div class="info_standart_news_card">
            <div class="view_news">
                <img src="{THEME}/images/icons/view.png" alt="">
                {views}
            </div>
            <div class="rating_like mt-2">
                [rating-type-2]
                    [rating-plus]
                    <svg class="bi bi-heart-fill" width="24px" height="24px" viewBox="0 0 16 16" fill="#FF5555" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                      </svg>
                        {rating}
                    [/rating-plus]
                [/rating-type-2]
            </div>
        </div>
        <a href="{full-link}" class="moredetails">
            View Details
            <svg class="bi bi-arrow-right" width="1em" height="1em" viewBox="0 0 16 16" fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z"/>
                <path fill-rule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8z"/>
              </svg>
        </a>
    </div>
</div>