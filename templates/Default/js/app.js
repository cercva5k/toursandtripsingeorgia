$.getScript('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', function () {
    $('.top_tours_carousel').slick({
        dots: false,
        infinite: false,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 5000,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });
    $('.top_tours_arrows .top_tours_prev').click(function () {
        $('.top_tours_carousel').slick('slickPrev')
    })
    $('.top_tours_arrows .top_tours_next').click(function () {
        $('.top_tours_carousel').slick('slickNext')
    })

})

$.getScript('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', function () {
    $('.special_offer_carousel_inner').slick({
        dots: false,
        infinite: false,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true
    });
    $('.offer_prev').click(function () {
        $('.special_offer_carousel_inner').slick('slickPrev')
    })
    $('.offer_next').click(function () {
        $('.special_offer_carousel_inner').slick('slickNext')
    })

})