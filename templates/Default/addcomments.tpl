<div class="box mb-5">
	<div id="addcomment" class="addcomment">
		<div class="box_in">
			<h3 class="write_recommendation">Write Recommendation</h3>
			<ul class="ui-form">
			[not-logged]
				<li class="form-group combo">
					<div class="combo_field"><input placeholder="Fullname" type="text" name="name" id="name" class="wide" required></div>
					<div class="combo_field"><input placeholder="E-Mail" type="email" name="mail" id="mail" class="wide"></div>
				</li>
			[/not-logged]
				<li id="comment-editor">{editor}</li>    
			[recaptcha]
				<li>{recaptcha}</li>
			[/recaptcha]
			</ul>
			<div class="form_submit">
				<button class="btn btn-big" type="submit" name="submit" title="Submit">Submit</button>
			</div>
		</div>
	</div>
</div>